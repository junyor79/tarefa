import java.util.Scanner;

public class Pesoindice {
    public static void main(String[] args) {
        System.out.println("CALCULADORA DE IMC");

        int opcao;
        do {

            System.out.println("1- Digite o Seu Peso");
            System.out.println("2- Digite a Sua Altura");
            System.out.print("Digite a Opção Desejada (0 caso deseje sair): ");

            Scanner scanner = new Scanner(System.in);
            opcao = scanner.nextInt();
            processar(opcao);
        } while   (opcao != 0);
    }
        
        public static void processar(int opcao) {
            switch(opcao) {
                case 1: {
                    Scanner scanner = new Scanner(System.in);
                    System.out.println("CALCULANDO O SEU IMC");

                    System.out.print("Digite o seu peso: ");
                    double numero1 = scanner.nextDouble();

                    System.out.print("Digite a sua altura: ");
                    double numero2 = scanner.nextDouble();
                    double calculo = numero2 * numero2;
                    double resultado = numero1 / calculo;

                    System.out.println("O seu IMC é: " + resultado);
                
                    if ( resultado < 16 ){
                        System.out.println("Você está abaixo do peso !");
                        break; 

                    } else if ( 16.0 > resultado && resultado <= 24.9){
                        System.out.println("Você está no peso ideal !");
                        break;

                    } else if ( 24.9 > resultado && resultado <= 29.9){
                        System.out.println("Você está acima do peso !");
                        break;

                    } else if ( resultado > 30 ){
                        System.out.println("Você está muito acima do seu peso ideal, procure um médico !");
                    }
                    break;    
                
                }
                }
                }
                }
                
            
        
   
