import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);

            System.out.print("Qual a quantidade de rodas deseja para seu veículo?: ");
               int qtdrodas = scanner.nextInt();

               System.out.print("Qual a quantidade de portas para seu veículo?: ");
               int qtdportas = scanner.nextInt();

               System.out.print("Qual cor deseja para seu veículo?: ");
               String cor = scanner.nextLine();

               System.out.print("Deseja ar-condicionado no veículo?: ");
               String arcondicionado = scanner.nextLine();

               System.out.print("Deseja som automotivo no veículo?: ");
               String somautomotivo = scanner.nextLine();

               Carros.Caracteristicas novaCaracteristica = new Carros.Caracteristicas();
               novaCaracteristica.setqtdrodas(qtdrodas);
               novaCaracteristica.setqtdportas(qtdportas);
               novaCaracteristica.setarcondicionado(arcondicionado);
               novaCaracteristica.setsomautomotivo(somautomotivo);
               novaCaracteristica.setcor(cor);

               System.out.println("Carro solicitado com sucesso ! ");
               
               System.out.println("Cor do Veículo: " + novaCaracteristica.getcor());
               System.out.println("Quantidade de Rodas: "+ novaCaracteristica.getqtdrodas());
               System.out.println("Quantidade de Portas: " + novaCaracteristica.getqtdportas());
               System.out.println("Carro com ar Condicionado ?: " + novaCaracteristica.getarcondicionado());
               System.out.println("Carro com Som Automotivo ?: " + novaCaracteristica.getsomautomotivo());
                    
        }

    }