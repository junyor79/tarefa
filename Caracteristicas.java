package Carros;

public class Caracteristicas {
    private int qtdrodas;
    private int qtdportas;
    private String cor;
    private String arcondicionado;
    private String somautomotivo;

    public void setqtdrodas(int qtdrodas) {
        this.qtdrodas = qtdrodas;
    }

    public void setqtdportas(int qtdportas) {
        this.qtdportas = qtdportas;
    }

    public void setcor(String cor) {
        this.cor = cor;
    }

    public void setarcondicionado(String arcondicionado) {
        this.arcondicionado = arcondicionado;
    }

    public void setsomautomotivo(String somautomotivo) {
        this.somautomotivo = somautomotivo;
    }

    public int getqtdrodas() {
        return qtdrodas;
    }

    public int getqtdportas() {
        return qtdportas;
    }

    public String getcor() {
        return cor;
    }

    public String getarcondicionado() {
        return arcondicionado;
    }

    public String getsomautomotivo() {
        return somautomotivo;
    }
}
